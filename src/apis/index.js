const base_url = "https://randomuser.me";

const instance = {
  post: async (path, payload = {}) => {
    const response = await fetch(`${base_url}${path}`, {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: new Headers({
        'Content-Type': 'application/json; charset=UTF-8',
      }),
    });

    return await response.json();
  },
  get: async (path, args = {}) => {
    const response = await fetch(`${base_url}${path}?` + new URLSearchParams({ ...args }), {
      method: 'GET',
    });

    return await response.json();
  },
};

import { UserApi } from '@apis/modules/user';

export { UserApi, instance }


// import axios from 'axios';
// import { getCookie } from '@/utils/cookies';

// const instance = axios.create({
//   // eslint-disable-next-line no-undef
//   baseURL: 'http://localhost:80/v1' //ToDo
// });

// // Setting bearer from cookie before every api call
// instance.interceptors.request.use((config) => {
//   const jwt = getCookie('jwt');
//   config.headers.Authorization = `Bearer ${jwt}`;

//   return config;
// });

// export { UserApi } from './modules/user';

// export default instance;
