import { instance } from '@apis/index';

export const UserApi = {
  getUser(payload) {
    return instance.get('/api', payload);
  },
};
