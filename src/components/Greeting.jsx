import { useState } from 'preact/hooks';

import { useStore } from '@nanostores/preact';
import { counter, incrementCounter } from '@stores/store';
export default function Greeting({messages}) {
  const $counter = useStore(counter);

  const randomMessage = () => messages[(Math.floor(Math.random() * messages.length))];
  
  const [greeting, setGreeting] = useState(messages[0]);

  return (
    <div> 
      <h3 class="text-red-900">{greeting}! Thank you for visiting!</h3>
      <button onClick={() => setGreeting(randomMessage())}>
        New Greeting
      </button>
      <div>
        <button onClick={() => incrementCounter({increment:-1})}>-</button>
        <p>{$counter}</p>
        <button onClick={() => incrementCounter({ increment: 1})}>+</button>
      </div>
    </div>
  );
}