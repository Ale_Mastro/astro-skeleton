import { atom, map } from 'nanostores';

export const counterInit = atom(0);

export const counter = map(0);
export function incrementCounter({ increment }) {
  counter.set(counter.get() + increment);
}